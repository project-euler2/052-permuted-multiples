import time
start_time = time.time()

def permuted_multiples():
    i = 1
    while True:
        if set(str(i*2)) == set(str(i*3)) == set(str(i*4)) == set(str(i*5)) == set(str(i*6)):
            return i
        i+=1 

print(permuted_multiples())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )